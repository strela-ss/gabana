# coding=utf-8
from __future__ import unicode_literals

import sqlite3
import kivy
from kivy.uix.textinput import TextInput
from kivy.uix.vkeyboard import VKeyboard

kivy.require('1.9.1')

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.graphics import Rectangle


class MaxInput(TextInput):
    def __init__(self, **kwargs):
        super(MaxInput, self).__init__(**kwargs)
        self.max_length = kwargs['max_length']

    def insert_text(self, substring, from_undo=False):
        s = self.text
        if len(s) > self.max_length:
            s = ''
        else:
            s = substring
        return super(MaxInput, self).insert_text(s, from_undo=from_undo)

    def _on_focus(self, instance, value, *largs):
        self.background_normal = 'img/parts/field.png'
        return super(MaxInput, self)._on_focus(instance, value, *largs)


class NumKeyboard(VKeyboard):
    def __init__(self, **kwargs):
        super(NumKeyboard, self).__init__(**kwargs)
        self.layout = 'numeric.json'
        self.size = (700, 70)


class UkKeyboard(VKeyboard):
    def __init__(self, **kwargs):
        super(UkKeyboard, self).__init__(**kwargs)
        self.layout = 'uk.json'


class PhoneInput(MaxInput):
    def on_focus(self, instance, value, *largs):
        win = self.get_root_window()
        if win:
            win.release_all_keyboards()
            win._keyboards = {}
            if value:  # User focus; use special keyboard
                win.set_vkeyboard_class(NumKeyboard)
            else:  # User defocus; switch back to standard keyboard
                win.set_vkeyboard_class(VKeyboard)

    def insert_text(self, substring, from_undo=False):
        s = ''.join(filter(unicode.isdigit, list(substring)))
        return super(PhoneInput, self).insert_text(s, from_undo=from_undo)


class UkTextInput(MaxInput):
    def on_focus(self, instance, value, *largs):
        win = self.get_root_window()
        if win:
            win.release_all_keyboards()
            win._keyboards = {}
            if value:
                win.set_vkeyboard_class(UkKeyboard)
            else:
                win.set_vkeyboard_class(VKeyboard)


class FormLayout(FloatLayout):
    def __init__(self, **kwargs):
        self.root = kwargs['root']
        super(FormLayout, self).__init__(**kwargs)
        default_kwargs = {
            'font_size': '23sp',
            'halign': 'left',
            'color': [0, 0, 0, 1],
            'size_hint': (.45, None),
        }
        self.add_widget(Label(
            text='Залиште свої контактні дані*:',
            pos=(425, 775),
            bold=True,
            size_hint=(.5, None),
            font_size='30sp',
            color=[0, 0, 0, 1],
        ))
        self.add_widget(Label(
            text='ПІБ',
            pos=(288, 700),
            **default_kwargs
        ))
        self.add_widget(Label(
            text='Місто',
            pos=(301, 575),
            **default_kwargs
        ))
        self.add_widget(Label(
            text='Телефон',
            pos=(315, 450),
            **default_kwargs
        ))

        inpt_kwargs = {
            'multiline': False,
            'background_active': 'img/parts/field_active.png',
            'background_normal': 'img/parts/field.png',
            'border': [0, 0, 0, 0],
            'font_size': '25sp',
            'size_hint': (.28, .06),
            'padding': (20, 15),
            'cursor_color': [1, .55, 0, 1],
            'foreground_color': [1, .55, 0, 1],
            'x': 700,
        }
        self.inpts = [
            UkTextInput(y=670, max_length=64, **inpt_kwargs),
            UkTextInput(y=545, max_length=32, **inpt_kwargs),
            PhoneInput(y=420, max_length=9, hint_text='YYYXXXXXXX', **inpt_kwargs)
        ]
        map(lambda x: self.add_widget(x), self.inpts)

        self.submit = Button(
            pos=(790, 285),
            size=(349, 68),
            size_hint=(None, None),
            background_normal='img/parts/save_btn.jpg',
        )
        self.submit.bind(on_press=self.submit_callback)
        self.add_widget(self.submit)
        self.add_widget(Label(
            text='',
            font_size='10sp',
            color=[1, 1, 1, 1],
        ))

    def submit_callback(self, instanse):
        if len(filter(lambda x: x.text, self.inpts)) < 3:
            for i in self.inpts:
                if not i.text:
                    i.background_normal = 'img/parts/field_blank.png'
            return
        with sqlite3.connect('_db.db') as conn:
            cursor = conn.cursor()
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS user (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name VARCHAR(100),
              city VARCHAR(100),
              phone VARCHAR(20)
            )
            """)
            cursor.execute("""
            INSERT INTO user (name, city, phone)
            VALUES (?, ?, ?)
            """, list(map(lambda x: x.text, self.inpts)))
        self.root.next_screen()


class CheckboxApp(App):
    def build(self):
        self.root = root = FormLayout()
        root.bind(size=self._update_rect, pos=self._update_rect)

        with root.canvas.before:
            self.rect = Rectangle(size=root.size, pos=root.pos)
        return self.root

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


if __name__ == '__main__':
    CheckboxApp().run()
