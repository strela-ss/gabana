# coding=utf-8
from __future__ import unicode_literals

from copy import copy
from kivy import Config

Config.set('kivy', 'keyboard_layout', 'uk.json')
Config.set('kivy', 'keyboard_mode', 'dock')
import kivy

from conf import QUESTIONS
from form import FormLayout

kivy.require('1.9.1')

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.uix.vkeyboard import VKeyboard
from question_tab import QuestionTabLayout
from multiple_checkbox import QuestionLayout
from kivy.core.window import Window
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.graphics import Rectangle
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout


class AbstractScreen(FloatLayout):
    def __init__(self, **kwargs):
        self.root = kwargs.pop('root')
        super(AbstractScreen, self).__init__()
        self.add_widget(
            Image(
                source=kwargs.pop('bg_img'),
                size_hint=(1, 1),
                pos_hint={'center_x': .5, 'center_y': .5}))
        self.exit = Button(
            background_color=[0, 0, 0, 0],
            pos=(1635, 980),
            size=(250, 80),
            size_hint=(None, None),
        )
        self.exit.bind(state=self.exit_callback)
        self.add_widget(self.exit)

    def exit_callback(self, instance, value):
        if value == 'down':
            root.refresh()


class AbstractNextScreen(AbstractScreen):
    def __init__(self, **kwargs):
        super(AbstractNextScreen, self).__init__(**kwargs)
        self.submit = Button(
            pos=(630, 117),
            size=(260, 68),
            size_hint=(None, None),
            background_normal='img/parts/next_btn.png',
        )
        self.submit.bind(state=self.submit_callback)
        self.add_widget(self.submit)

    def submit_callback(self, instance, value):
        if value == 'down':
            self.root.next_screen()


class QuestionScreen(AbstractScreen):
    def __init__(self, **kwargs):
        self.num = kwargs['num']
        # Default background
        kwargs.update({
            'bg_img': 'img/q_blank.jpg'
        })
        super(QuestionScreen, self).__init__(**kwargs)
        del kwargs['root']
        # Image on the right side
        self.add_widget(
            Image(
                source=kwargs['q_part'],
                size_hint=(1, 1),
                pos_hint={
                    'center_x': .78,
                    'center_y': .45 if self.num in [1, 2] else .53
                }))
        # Array of questions
        self.q_tab_layout = QuestionTabLayout(qarr=self.root.qarr)
        self.add_widget(self.q_tab_layout)
        # Question and choices
        self.q_layout = QuestionLayout(
            root=self,
            **kwargs
        )
        self.add_widget(self.q_layout)

    def next_screen(self, result):
        self.root.qarr += result
        if self.num < 7:
            self.root.screens += [QuestionScreen(num=self.num + 1, root=self.root, **QUESTIONS[self.num])]
        else:
            self.remove_widget(self.q_tab_layout)
            self.add_widget(QuestionTabLayout(qarr=self.root.qarr), 1)
            self.root.finish(self)
            return
        Clock.schedule_once(self.root.next_screen, 1)


class Main(FloatLayout):
    def __init__(self, **kwargs):
        super(Main, self).__init__(**kwargs)
        self.add_widget(Root(root=self))

    def refresh(self):
        self.clear_widgets()
        self.add_widget(Root(root=self))


gif = Image(
    source='img/f_yes_1.ZIP',
    size_hint=(1, 1),
    anim_loop=1,
    anim_delay=.1,
    pos_hint={'center_x': .5, 'center_y': .5}
)


class Root(FloatLayout):
    def __init__(self, **kwargs):
        super(Root, self).__init__(**kwargs)
        self.root = kwargs.pop('root')
        self.mygif = Image(
            source='img/f_yes_1.ZIP',
            anim_delay=.1,
            anim_loop=1,
            size_hint=(1, 1),
            pos_hint={'center_x': .5, 'center_y': .5}
        )
        default_kwargs = {'root': self, }
        self.current_screen = 0
        self.qarr = ''
        self.screens = [
            Screen0(bg_img="img/s1.jpg", **default_kwargs),
            ScreenForm(bg_img='img/s_form.jpg', root=self),
            Screen1(bg_img="img/s2.jpg", **default_kwargs),
            Screen2(bg_img="img/s3.jpg", **default_kwargs),
            QuestionScreen(num=1, root=self, **QUESTIONS[0]),
        ]
        self.next_screen()

    def next_screen(self, dt=None):
        self.clear_widgets()
        self.add_widget(self.screens[self.current_screen])
        self.current_screen += 1

    def finish(self, widget):
        if self.qarr.count('1') > 3:
            widget.remove_widget(widget.children[-3])
            widget.add_widget(Image(
                source='img/f_yes_1.ZIP',
                anim_delay=.1,
                anim_loop=1,
                size_hint=(1, 1),
                pos_hint={'center_x': .5, 'center_y': .5}
            ), 2)
            self.screens += [
                ScreenFinalYes(bg_img='img/f_yes.jpg', root=self),
            ]
            t = 8
        else:
            self.screens += [
                ScreenFinalNo(bg_img='img/f_no.jpg', root=self),
            ]
            t = 1
        self.screens += [
            ScreenFinal(bg_img='img/se.jpg', root=self)
        ]
        Clock.schedule_once(self.next_screen, t)


class Screen0(AbstractScreen):
    def __init__(self, **kwargs):
        super(Screen0, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        self.root.next_screen()


class ScreenForm(AbstractScreen):
    def __init__(self, **kwargs):
        super(ScreenForm, self).__init__(**kwargs)
        self.add_widget(FormLayout(root=self))

    def next_screen(self):
        self.root.next_screen()


class Screen1(AbstractScreen):
    def __init__(self, **kwargs):
        super(Screen1, self).__init__(**kwargs)
        self.submit = Button(
            pos=(770, 570),
            size=(375, 68),
            size_hint=(None, None),
            background_normal='img/parts/start_btn.png',
        )
        self.submit.bind(state=self.submit_callback)
        self.add_widget(self.submit)

    def submit_callback(self, instance, value):
        if value == 'down':
            self.root.next_screen()


class Screen2(AbstractNextScreen):
    def __init__(self, **kwargs):
        super(Screen2, self).__init__(**kwargs)


class ScreenFinalNo(AbstractScreen):
    def __init__(self, **kwargs):
        super(ScreenFinalNo, self).__init__(**kwargs)
        self.submit = Button(
            pos=(633, 117),
            size=(260, 68),
            size_hint=(None, None),
            background_normal='img/parts/next_btn.png',
        )
        self.submit.bind(state=self.submit_callback)
        self.add_widget(self.submit)

    def submit_callback(self, instance, value):
        if value == 'down':
            self.root.next_screen()


class ScreenFinalYes(AbstractScreen):
    def __init__(self, **kwargs):
        super(ScreenFinalYes, self).__init__(**kwargs)
        self.submit = Button(
            pos=(633, 117),
            size=(260, 68),
            size_hint=(None, None),
            background_normal='img/parts/next_btn.png',
        )
        self.submit.bind(state=self.submit_callback)
        self.add_widget(self.submit)

    def submit_callback(self, instance, value):
        if value == 'down':
            self.root.next_screen()


class ScreenFinal(AbstractScreen):
    def __init__(self, **kwargs):
        super(ScreenFinal, self).__init__(**kwargs)
        self.submit = Button(
            pos=(765, 156),
            size=(392, 68),
            size_hint=(None, None),
            background_normal='img/parts/finish_btn.png',
        )
        self.submit.bind(state=self.submit_callback)
        self.add_widget(self.submit)

    def submit_callback(self, instance, value):
        if value == 'down':
            self.root.root.refresh()


class Gabana(App):
    def build(self):
        global root
        root = Main()
        root.bind(size=self._update_rect, pos=self._update_rect)

        with root.canvas.before:
            self.rect = Rectangle(size=root.size, pos=root.pos)
        return root

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


if __name__ == '__main__':
    Window.size = (1920, 1080)
    Window.allow_screensaver = False
    Window.fullscreen = True
    Gabana().run()
