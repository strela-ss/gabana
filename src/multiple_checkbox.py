# coding=utf-8
from __future__ import unicode_literals

import kivy
from kivy.clock import Clock
from kivy.uix.image import Image

kivy.require('1.9.1')

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.graphics import Rectangle


class CheckboxOneLayout(GridLayout):
    def __init__(self, **kwargs):
        super(CheckboxOneLayout, self).__init__(**kwargs)
        self.cols = 2
        self.padding = 40
        self.disable_img = kwargs['img']

        self.root = kwargs['root']
        self.btn = Button(
            active=False,
            size=(67, 67),
            width=67,
            height=67,
            size_hint=(None, None),
            background_normal='img/parts/radio_btn_empty.png',
            background_disabled_normal='img/parts/radio_btn_empty.png',
            background_down='img/parts/radio_btn_empty.png',
            background_disabled_down='img/parts/radio_btn_empty.png',
        )
        self.btn.bind(state=self.btn_pressed)
        lbl = Label(
            text='[color=000000]%s[/color]' % kwargs['label'],
            markup=True,
            halign="left",
            valign="top",
            size_hint=(1, None),
            font_size=kwargs.get('choice_font_size'),
        )
        lbl.text_size = (400, None)
        lbl.height = lbl.texture_size[1]
        self.add_widget(self.btn)
        self.add_widget(lbl)

    def deactivate(self):
        self.btn.disabled = True
        self.btn.canvas.ask_update()

    def btn_pressed(self, instance, value):
        if value == 'normal':
            instance.background_down = self.disable_img
            instance.background_disabled_normal = self.disable_img
            self.root.deactivate('1' if instance.background_disabled_normal == 'img/parts/radio_btn_yes.png' else '0')


class ImageCheckbox(FloatLayout):
    def __init__(self, **kwargs):
        super(ImageCheckbox, self).__init__(**kwargs)
        self.root = kwargs['root']
        self.disable_img = kwargs['img']
        self.btn = Button(
            active=False,
            size=(67, 67),
            pos=(530, 470 - 230*kwargs['num']),
            size_hint=(None, None),
            background_normal='img/parts/radio_btn_empty.png',
            background_down='img/parts/radio_btn_empty.png',
            background_disabled_normal='img/parts/radio_btn_empty.png',
            background_disabled_down='img/parts/radio_btn_empty.png',
        )
        self.btn.bind(state=self.btn_pressed)
        lbl = Image(
            source=kwargs['label'],
            size=(406, 215),
            pos=(600, 400 - 250*kwargs['num']),
            size_hint=(None, None),
        )
        self.add_widget(self.btn)
        self.add_widget(lbl)

    def deactivate(self):
        self.btn.disabled = True
        self.btn.canvas.ask_update()

    def btn_pressed(self, instance, value):
        if value == 'normal':
            instance.background_down = self.disable_img
            instance.background_disabled_normal = self.disable_img
            self.root.deactivate('1' if instance.background_disabled_normal == 'img/parts/radio_btn_yes.png' else '0')


class CheckboxLayout(StackLayout):
    def __init__(self, **kwargs):
        data = kwargs.pop('data')
        self.root = kwargs.pop('root')
        self.spacing = kwargs.pop('spacing')
        super(CheckboxLayout, self).__init__(**kwargs)
        self.btns = []
        for idx, i in enumerate(data):
            pressed_img = 'img/parts/radio_btn_yes.png' if kwargs['answer'] == idx else 'img/parts/radio_btn_no.png'
            new_kwargs = {
                'root': self,
                'img': pressed_img,
                'label': i,
                'size_hint': (1, None),
            }
            if kwargs['data_type'] == 'img':
                checkbox = ImageCheckbox(
                    num=idx,
                    **new_kwargs
                )
            else:
                checkbox = CheckboxOneLayout(
                    choice_font_size=kwargs.get('choice_font_size'),
                    **new_kwargs
                )
            self.add_widget(checkbox)
            self.btns += [checkbox]

    def deactivate(self, result):
        for i in self.btns:
            i.deactivate()
        Clock.schedule_once(lambda x: self.root.next_screen(result), 0)


class QuestionLayout(FloatLayout):
    def __init__(self, **kwargs):
        self.pos = (480, 192)
        self.size = (560, 625)
        self.size_hint = (None, None)
        self.root = kwargs['root']
        super(QuestionLayout, self).__init__(**kwargs)
        lbl = Label(
            text='[color=000000][size=30]%s[/size][/color]' % kwargs['title'],
            markup=True,
            bold=True,
            halign="left",
            valign="top",
            size_hint=(1, None),
            x=490,
            y=kwargs.get('pos_label', 750),
        )
        lbl.text_size = (500, None)
        lbl.height = lbl.texture_size[1]
        self.add_widget(lbl)
        self.choices = CheckboxLayout(
            root=self.root,
            data=kwargs['data'],
            data_type=kwargs.get('data_type', 'label'),
            answer=kwargs['answer'],
            spacing=kwargs.get('spacing', 40),
            size_hint=(1, .7),
            x=490,
            y=kwargs.get('pos_choices', 250),
            choice_font_size=kwargs.get('choice_font_size', '26sp'),
        )
        self.add_widget(self.choices)

    def get_result(self):
        return self.choices.get_result()


class CheckboxApp(App):
    def build(self):
        self.root = root = QuestionLayout(
            title='Title long long long long long long \nlong long long long long long long long',
            data=[
                'Choice 1',
                'Choice 2',
                'Choice long long long long long long \nlong long long long long long long long',
            ]
        )
        root.bind(size=self._update_rect, pos=self._update_rect)

        with root.canvas.before:
            self.rect = Rectangle(size=root.size, pos=root.pos)
        return self.root

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


if __name__ == '__main__':
    CheckboxApp().run()
