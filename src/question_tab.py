# coding=utf-8
from __future__ import unicode_literals

import kivy

kivy.require('1.9.1')

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.graphics import Rectangle


class QuestionTabLayout(FloatLayout):
    def __init__(self, **kwargs):
        self.qarr = qarr = kwargs.get('qarr', '')
        if len(qarr) < 7:
            self.qarr = (qarr + '!').ljust(7, '-')
        super(QuestionTabLayout, self).__init__(**kwargs)
        for i, answer in enumerate(self.qarr):
            if answer == '-':
                self._add_num(i, 1)
            elif answer == '0':
                self._add_num(i, 2)
            elif answer == '1':
                self._add_num(i, 3)
            else:
                self._add_num(i, 4)

    def _add_num(self, num, state):
        pos_arr = [
            (1235, 470),
            (1202, 640),
            (1267, 805),
            (1457, 877),
            (1649, 805),
            (1718, 640),
            (1679, 470),
        ]
        self.add_widget(
            Image(
                source='img/nums/n%s_s%s.png' % (num + 1, state),
                size_hint=(None, None),
                size=(87, 85),
                pos=pos_arr[num],
            )
        )


class CheckboxApp(App):
    def build(self):
        self.root = root = QuestionTabLayout(bg_img='img/q1.jpg')
        root.bind(size=self._update_rect, pos=self._update_rect)

        with root.canvas.before:
            self.rect = Rectangle(size=root.size, pos=root.pos)
        return self.root

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size


if __name__ == '__main__':
    Window.size = (1920, 1080)
    Window.allow_screensaver = False
    Window.fullscreen = True
    CheckboxApp().run()
